package com.kazale.appjh2.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "subscriber")
public class Subscriber {
	@Id
	@GeneratedValue
	private Long id;
	private String fisrt;
	private String last;
	private String email;
	
	
	
	public Subscriber() {
	}

	public Subscriber(String fisrt, String last, String email) {
		super();
		this.fisrt = fisrt;
		this.last = last;
		this.email = email;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFisrt() {
		return fisrt;
	}
	public void setFisrt(String fisrt) {
		this.fisrt = fisrt;
	}
	public String getLast() {
		return last;
	}
	public void setLast(String last) {
		this.last = last;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "Subscriber [id=" + id + ", fisrt=" + fisrt + ", last=" + last + ", email=" + email + "]";
	}
	
}
