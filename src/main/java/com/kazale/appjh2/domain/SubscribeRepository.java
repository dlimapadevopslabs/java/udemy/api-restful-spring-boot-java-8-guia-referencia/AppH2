package com.kazale.appjh2.domain;

import org.springframework.data.repository.CrudRepository;

public interface SubscribeRepository extends CrudRepository<Subscriber, Long>{

}
