package com.kazale.appjh2;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.kazale.appjh2.domain.SubscribeRepository;
import com.kazale.appjh2.domain.Subscriber;

@SpringBootApplication
public class AppH2Application {

	public static void main(String[] args) {
		SpringApplication.run(AppH2Application.class, args);
	}
	
	@Bean
	public CommandLineRunner commandLineRunner(SubscribeRepository repository) {
		return args -> {
			System.out.println("### persistindo linha no banco...");
			repository.save(new Subscriber("daniel", "lima", "dlima@tre-pa.gov.br"));
			
			System.out.println("### recuperando linha...");
			Subscriber subscriber = repository.findOne(1L);
			System.out.println(subscriber.toString());

			System.out.println("### deletendo linha...");
			repository.delete(1L);
			Iterable<Subscriber> listaSubscriber = repository.findAll();
			System.out.println("### qtd de linhas: " + listaSubscriber.spliterator().getExactSizeIfKnown());
		};
	}
}
